
const apiKey = '4e96d7010f2eed59c0c43e5f1543cb0c';
const api = 'https://api.themoviedb.org/3/movie/550?api_key=4e96d7010f2eed59c0c43e5f1543cb0c';

const apiTopRated = `https://api.themoviedb.org/3/movie/top_rated?api_key=${apiKey}`;
const apiSearch = `https://api.themoviedb.org/3/search/movie?api_key=${apiKey}`; // search by movies name
const apiSearchByPeople = `https://api.themoviedb.org/3/search/person?api_key=${apiKey}`;
const apiDetail = `https://api.themoviedb.org/3/movie/`;
const apiDetailPerson = `https://api.themoviedb.org/3/person/`;
const img_prefix = `https://image.tmdb.org/t/p/w500`;
const omdb = ` http://www.omdbapi.com/?apikey=3f6aef3c`;

var current_page = 1;
var current_pageSearch = 1;
async function loadPage() {
  
  var response = await fetch(apiTopRated);
  var rs = await response.json();
  return rs;
}

async function evtSubmit() {
 
  //e.preventDefault();
  var strSearch = $('form input').val();
  var reqStr =  apiSearch+'&query='+strSearch;
  var response = await fetch(reqStr);
  var rs = await response.json();
  return rs;
}

function fillMovies(ms) {
  $('#main').empty();
 $('#reviews').empty();
 if(ms != ''){
  for (const m of ms) {
    var src1 = `${img_prefix}${m.poster_path}`;
    $('#main').append(`
    <div class="col-md-3 py-3">
      <div class="card shadow h-100" onclick="loadDetail(${m.id})">
        <img src=`+ src1 + ` class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">${m.title}</h5>
          <div class="card-text">
          Release date: ${m.release_date}
          <br />Vote count: ${m.vote_count} votes
          <br />Popularity: ${m.popularity}%
          </div>
          
        </div>
      </div>
    </div>`
    )
  }
}
else
{
  $('#main').append(`<strong class="text-warning" style="font-size:20px;">We don't have information about this movie</strong>`);
}
}

function loading() {
  $('#main').empty();
  $('#main').append(`<div class="text-center">
  <div class="spinner-border" role="status">
    <span class="sr-only">Loading...</span>
  </div>
</div>`);
}

async function loadDetail(id) {
  $(btn_prev).hide();
  $(btn_next).hide();
  $(btn_prevSearch).hide();
  $(btn_nextSearch).hide();
  $(page).hide();
  const reqStr = `${apiDetail}${id}?api_key=${apiKey}`;
  loading();
  const response = await fetch(reqStr);
  const rs = await response.json();

  fillMovie(rs);
  seeReview(id);
}

async function load_imdb(id) {
  const reqStr = `${omdb}&i=${id}`;
  const response = await fetch(reqStr);
  const rs = await response.json();
  return rs;
};

async function info(name) {
  $("#reviews").empty();
  const reqStr = `${apiSearchByPeople}&query=${name}`;
  loading();
  const response = await fetch(reqStr);
  const rs = await response.json();
  const id_Star = rs.results[0].id;
  const req2 = `${apiDetailPerson}${id_Star}?api_key=${apiKey}`;
  const response2 = await fetch(req2);
  const rs2 = await response2.json();
  infoStar(rs2);
  infoStar2(rs.results[0].known_for);
}

function infoStar(m) {
  $('#main').empty();
  $('#main').append(`
<!-- Page Content -->
<div class="container">
  <!-- Portfolio Item Row -->
  <div class="row">
    <div class="col-md-4">
      <img class="img-fluid" src="${img_prefix}${m.profile_path}"  alt="..." >
    </div>

    <div class="col-md-8">
      <h3 class="my-3">Information</h3>
      <ul>
        <li><strong>Name: </strong>${m.name}</li>
        <li><strong>Birthday: </strong>${m.birthday}</li>
        <li><strong>Place of birth: </strong>${m.place_of_birth}</li>
        <li><strong>Biography: </strong>${m.biography}</li>
      </ul>
    </div>

  </div>
  <!-- /.row -->
  <h3 class="my-4">Related movies</h3>
</div>


<!-- /.container -->
`)
}
function infoStar2(ms) {
  for (const m of ms) {
    var src1 = `${img_prefix}${m.poster_path}`;
    $('#main').append(`

  <div class="col-md-3 py-3">
    <div class="card shadow h-100" onclick="loadDetail(${m.id})">
      <img src=`+ src1 + ` class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">${m.title}</h5>
        <div class="card-text">
        Release date: ${m.release_date}
        <br />Vote count: ${m.vote_count} votes
        <br />Popularity: ${m.popularity}%
        </div>
        
      </div>
    </div>
  </div>`
    )
  }
}
async function fillMovie(m) {
  $('#main').empty();
  const img_src = `${img_prefix}${m.poster_path}`;
  const s = await load_imdb(`${m.imdb_id}`);
  let token = (s.Actors).split(', ');
  $('#main').append(`
  <!-- Page Content -->
<div class="container">
  <!-- Portfolio Item Row -->
  <div class="row">
    <div class="col-md-4">
      <img class="img-fluid" src=`+ img_src + `  alt="..." width="400px">
      <div class="my-4 text-center" style="font-size:20px; font-weight:bold;">${m.title}</div>
    </div>
   
    <div class="col-md-8">
      <h3 class="my-1">Overview</h3>
      <p>${m.overview}</p>
      <h3 class="my-1">Stars</h3>
      <ul>
        <li >${token[0]}<a onclick="info('${token[0]}')" href="#" style="font-size:12px;"> Detail</a></li>
        <li >${token[1]}<a onclick="info('${token[1]}')" href="#" style="font-size:12px;"> Detail</a></li>
        <li >${token[2]}<a onclick="info('${token[2]}')" href="#" style="font-size:12px;"> Detail</a></li>
        <li >${token[3]}<a onclick="info('${token[3]}')" href="#" style="font-size:12px;"> Detail</a></li>
        
      </ul>
      <h3 class="my-1">Director</h3>
      <span>${s.Director}</span>
      <h3 class="my-1">Year of manufacture</h3>
      <span>${s.Year}</span>
      <h3 class="my-1">Genre</h3>
      <span>${s.Genre}</span>
    </div>
    <h1>Reviews</h1>
  </div>
  <!-- /.row -->
</div>
<!-- /.container -->
  `)
}

async function seeReview(id){
  var reqStr = `${apiDetail}${id}/reviews?api_key=${apiKey}`;
  //loading();
  var response = await fetch(reqStr);
  var rs = await response.json();
  var reviews = rs.results;
  if(reviews.length != 0){
  for (const m of reviews) {
  $("#reviews").append(`
  <li class="media">             
              <div>                  
                  <strong class="text-success" style="font-size:20px;">${m.author}</strong>
                  <p>
                      ${m.content}
                  </p>
              </div>
          </li>   
  `)
  }
}
else{
  $("#reviews").append(`              
           <strong class="text-warning" style="font-size:20px;">This movie hasn't have any reviews</strong>                
  `)
}
}
function prevPage()
{
    if (current_page > 1) {
        current_page--;
        changePage(current_page);
    }
}
async function nextPage()
{
  var rs = await loadPage();
  var numPages = rs.total_pages;
    if (current_page < numPages) {
        current_page++;
        changePage(current_page);
    }
}
async function loadMoviesPerPage(currentPage) {
      var req = apiTopRated + '&page=' + currentPage;
      const response = await fetch(req);
      var data =  await response.json();
      fillMovies(data.results);
    }
   
async function changePage(page)
{
  var btn_next = $("#btn_next");
var btn_prev = $("#btn_prev");
var listing_table = $("#main");
var page_span = $("#page");
  $("#btn_prevSearch").hide();
  $("#btn_nextSearch").hide();
    // Validate page
    loading();
    var rs = await loadPage();
  var numPages = rs.total_pages;
    if (page < 1) page = 1;
   
    if (page > numPages) page = numPages;
   
    loadMoviesPerPage(page);
    page_span.empty();
    page_span.append(page + "/" + numPages);

    if (page == 1) {
        btn_prev.css('visibility', 'hidden');
    } else {
        btn_prev.css('visibility', 'visible');
    }

    if (page == numPages) {
        btn_next.css('visibility', 'hidden');
    } else {
      btn_next.css('visibility', 'visible');
    }
}
//-----------------------------------------------
//-----------------------------------------------
function prevPageSearch()
{
    if (current_pageSearch > 1) {
      current_pageSearch--;
        changePageSearch(current_pageSearch);
    }
}
async function nextPageSearch()
{
  var rs = await evtSubmit();
  var numPages = rs.total_pages;
    if (current_pageSearch < numPages) {
      current_pageSearch++;
        changePageSearch(current_pageSearch);
    }
}
async function loadMoviesPerPageSearch(current_pageSearch) {
     // var req = `${apiSearch}&query=${strSearch}&page=` + currentPage;
     var strSearch = $('form input').val();
      var req =  apiSearch+'&query='+strSearch+'&page='+current_pageSearch;
      const response = await fetch(req);
      var data =  await response.json();
      fillMovies(data.results);
    }
   
async function changePageSearch(page)
{  
  var btn_next = $("#btn_nextSearch");
  var btn_prev = $("#btn_prevSearch");  
  var listing_table = $("#main");
  var page_span = $("#page");

  $("#btn_prev").hide();
  $("#btn_next").hide();
  $("#btn_prevSearch").show();
  $("#btn_nextSearch").show();
 // $("page").show();

    // Validate page
    loading();
    var rs = await evtSubmit();
  var numPages = rs.total_pages;
    if (page < 1) page = 1;
   
    if (page > numPages) page = numPages;
   
    loadMoviesPerPageSearch(page);
    page_span.empty();
    page_span.append(page + "/" + numPages).show();

    if (page == 1) {
      btn_prev.css('visibility', 'hidden');
    } else {
        btn_prev.css('visibility', 'visible');
    }

    if (page == numPages) {
        btn_next.css('visibility', 'hidden');
    } else {
      btn_next.css('visibility', 'visible');
    }
}

function changeFunc() {
  var selectBox = $("#selectBox");
  var selectedValue = selectBox.children("option:selected").val();
  current_pageSearch = 1;
  if(selectedValue == "1")
    changePageSearch(1);
    else
    searchByStar();
    selectBox.prop("selectedIndex", 0);
 }

 async function searchByStar(){
  var strSearch = $('form input').val();
  var reqStr = `${apiSearchByPeople}&query=${strSearch}`;
  $("#btn_prev").hide();
  $("#btn_next").hide();
  $("#btn_prevSearch").hide();
  $("#btn_nextSearch").hide();
  $("#page").hide();
  loading();
  const response = await fetch(reqStr);
  const rs = await response.json();
  fillMovies(rs.results[0].known_for);
 }